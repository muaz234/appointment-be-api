
const { Sequelize } = require('sequelize');
const AppointmentModel = require('../models/appointment')
const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASSWORD, { 
    host: process.env.DB_HOST, 
    dialect: 'mysql', 
    dialectOptions: {
      ssl: {
        rejectUnauthorized : false,
      }
    }});

    const db = {};
    db.Sequelize = Sequelize
    db.sequelize = sequelize
    db.sequelize.sync();
    const Appointment = AppointmentModel(sequelize, Sequelize);
    module.exports = { Appointment };