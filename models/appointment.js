const { Sequelize, DataTypes } = require('sequelize');



module.exports = (sequelize, type) => {

    const Appointment = sequelize.define('Appointment', 
    {
        name: { type: DataTypes.STRING, allowNull: false },
        email: { type: DataTypes.STRING, allowNull: true },
        pic: { type: DataTypes.STRING, allowNull: false },
        contact_no: { type: DataTypes.STRING, allowNull: false },
        status: { type: DataTypes.TINYINT, allowNull: true},
        appointment_date: { type: DataTypes.DATE, allowNull: false },
    }, 
    { timestamps: true });

    return Appointment;

}

   


