var createError = require('http-errors');
var express = require('express');
var cors = require('cors');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var http = require('http');
require('dotenv').config()

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();
var server = http.createServer(app);
// allowed domain to prevent cors
  // add more domain here    
var allowedOrigins = ['http://localhost:3001', 'http://192.168.1.2:3001/', 'https://appointment-sofax.muazahmed.me'];
// add more methods here
var methods_allowed = ['GET','POST','PUT'];
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
// enable cors
app.use(cors({ origin: allowedOrigins, methods: methods_allowed }));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

server.listen(3000);
console.log('Express server started on port %s', server.address().port);

module.exports = app;
